import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, BehaviorSubject } from 'rxjs';
import { map, tap } from 'rxjs/operators';

interface ViewDetail {
  age: number;
  region: string;
  date: string;
}

export interface Video {
  title: string;
  author: string;
  id: string;
  viewDetails: ViewDetail[];
}

function embiggenAuthorNames(videos: Video[]): Video[] {
  return videos.map(
    video => ({ ...video, author: video.author.toUpperCase() })
  );
}

@Injectable({
  providedIn: 'root'
})
export class VideoDataService {

  private _selectedVideo: Video;

  selectedVideo = new BehaviorSubject(null);

  constructor(private http: HttpClient) {}

  selectVideo(video: Video): void {
    this.selectedVideo.next(video);
  }

  loadVideos(): Observable<Video[]> {
    return this.http
      .get<Video[]>('http://api.angularbootcamp.com/videos')
      .pipe(
        map(videoData => embiggenAuthorNames(videoData)),
        tap(videoData => this.selectedVideo.next(videoData[0]))
      )
    ;
  }
}
