import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { VideoDataService, Video } from '../../video-data.service';

@Component({
  selector: 'app-video-dashboard',
  templateUrl: './video-dashboard.component.html',
  styleUrls: ['./video-dashboard.component.scss']
})
export class VideoDashboardComponent implements OnInit {

  videos: Observable<Video[]>;
  
  constructor(private svc: VideoDataService) { }

  ngOnInit() {
    this.videos = this.svc.loadVideos();
  }

}
