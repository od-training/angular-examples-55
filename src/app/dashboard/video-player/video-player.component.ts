import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { VideoDataService, Video } from '../../video-data.service';

@Component({
  selector: 'app-video-player',
  templateUrl: './video-player.component.html',
  styleUrls: ['./video-player.component.scss']
})
export class VideoPlayerComponent implements OnInit {
  
  selectedVideo: Observable<Video>;

  constructor(private svc: VideoDataService) {
    this.selectedVideo = svc.selectedVideo.pipe(
      tap( video => console.log('Got newly-selected video in PLAYER:', video) )
    );
  }

  ngOnInit() {
  }

}
