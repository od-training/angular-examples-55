import { Component, OnInit, Input } from '@angular/core';

import { Observable } from 'rxjs';
import { tap, shareReplay } from 'rxjs/operators';

import { VideoDataService, Video } from '../../video-data.service';

@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.scss']
})
export class VideoListComponent implements OnInit {
  @Input() videos: Video[] = [];

  selectedVideo: Observable<Video>;

  constructor(private svc: VideoDataService) {
    this.selectedVideo = svc.selectedVideo.pipe(
      tap( video => console.log('Got newly-selected video in LIST:', video) ),
      shareReplay()
    );
  }

  ngOnInit() {
  }
}
