import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-stat-filters',
  templateUrl: './stat-filters.component.html',
  styleUrls: ['./stat-filters.component.scss']
})
export class StatFiltersComponent implements OnInit {

  fg: FormGroup;

  partialTitle: FormControl;
  
  constructor(fb: FormBuilder) {
  
    this.fg = fb.group({
      partialTitle: ['', Validators.minLength(3)],
      isCaseSensitive: [false]
    });

    this.partialTitle = this.fg.controls.partialTitle as FormControl;
  }

  ngOnInit() {
  }

}
